bindkey "^R" history-incremental-search-backward

printcolors()
{
    for i in {0..254}; do
        print -P "$i: %F{$i}Hello world!%f";
    done
}

case $(hostname) in
    *"lab"*) color=13;;
    "ic") color=200;;
    "mithaokhta") color=51;;
    *"pi") color=231;;
    "gx"*) color=87;;
    *"cat"*) color=82;;
esac
PROMPT="%B%F{$color}%n@%m%f%F{49}[%~]"$'\n'">%f%b"
