#!/bin/bash

echo "Welcome Turth, installing system configuration"

set -eu -o pipefail

ensure_line_in_file() {
    line="$1"
    file="$2"

    if ! [ -f "$file" ]; then 
        # File does not exist, move on
        echo "File $file does not exist, skipping"
        return
    fi
       
    if ! grep -q "$line" "$file"; then
        echo "Line [$line] was not detected in [$file] appending it"
        echo "$line" >> "$file"
    else
        echo "Line [$line] present in [$file]"
    fi
}

conf_dir=$(pwd)
ensure_line_in_file "source $conf_dir/common.sh" "$HOME/.zshrc"
ensure_line_in_file "source $conf_dir/common.sh" "$HOME/.bashrc"
ensure_line_in_file "source $conf_dir/zsh.sh" "$HOME/.zshrc"
ensure_line_in_file "source $conf_dir/nvim.vim" "$HOME/.config/nvim/init.vim"

read -p "Install elisp files?" answer
case "$answer" in 
    [yY] ) cp init.el tmacs.el ~/.emacs.d;;
    [nN] ) echo "Not installing";;
        *) echo "Unknown answer";;
esac

echo "Enjoy!"
