#!/bin/bash
export EDITOR="vim"
# set -o vi

# Some more ls aliases
alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'
alias lah="ls -lah"

#alias path="echo $PATH"
#alias watvpn="sudo openconnect -v cn-vpn.uwaterloo.ca"
alias ...="cd ../.."
alias ..="cd .."
alias :q="exit"

alias clipboard="xclip -selection c"
alias dlmus='youtube-dl --extract-audio --audio-format mp3 --audio-quality 0'
alias dstamp="date +%Y-%m-%d"
alias fastkbd="xset r rate 200 50"
alias fastpad="xinput --set-prop 'Synaptics TM3471-010' 190 2 0 0 0 2 0 0 0 1"
alias fk=fastkbd
alias intu="curl turth.space/install.sh | bash"
alias nocap="setxkbmap -option ctrl:nocaps"
alias notebook="jupyter notebook &>/dev/null &"
alias pdb="python3 -m pdb"
alias pwease=sudo
alias py="python3"
alias sbrc="source ~/.bashrc"
alias slowmouse="xinput --set-prop \"Logitech USB Receiver\" 155 0.5 0 0 0 0.5 0 0 0 1"
alias szrc="source ~/.zshrc"

alias ca=cargo
alias em=emacs

is_dir_in_path() {
    for path_dir in $(echo $PATH | tr ':' ' ' ); do
        if [ "$path_dir" = "$1" ]; then
            return 0
        fi
    done
    return 1
}

ensure_in_path_if_exists() {
    for desired_dir in "$@"; do
        if [ -d "$desired_dir" ] && ! is_dir_in_path "$desired_dir" ; then
            export PATH="$PATH:$desired_dir"
        fi
    done
}

wanted_path_dirs=( /sbin ~/bin ~/devel/utility ~/go/bin )
for wanted_dir in "${wanted_path_dirs[@]}"; do
    ensure_in_path_if_exists "$wanted_dir"
done

if which git &>/dev/null
then
    git config --global alias.aa 'add --all'
    git config --global alias.cam 'commit -a -m'
    git config --global alias.cm 'commit -m'
    git config --global alias.ch 'checkout'
    git config --global alias.s 'status'
    git config --global alias.b 'branch'
    git config --global alias.r 'remote'
    git config --global alias.p 'push'
    git config --global alias.reee 'reset --hard'
fi

gitmr() {
    git add --all
    git commit -m "$*"
    git push
}

gitc() {
    git add --all
    git commit -m "$*"
}

sdllibs() {
    echo "-lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_gfx -lSDL2_net"
}

trash() {
    trashdir="$HOME/.trash"
    if [ ! -d "$trashdir" ]; then
        mkdir "$trashdir";
    fi

    if [ $# -eq 0 ]; then
        echo "Usage: trash <trash>";
    else
        for garbage in "$@"; do
            echo "Trashing: $garbage";
            mv "$garbage" "$trashdir";
        done
    fi
}

allcat() {
    # This is wrong
    for f in *; do
        if [ -f "$f" ]; then
            echo "\n$f";
            cat "$f";
        fi
    done
}

orgwords() {
    # This will break if the filenames have spaces
    echo "There are" $(cat ~/Documents/zet/*.org | strings | wc -w) "words in your slipbox."
}

orgcards() {
    echo "You have" $(cat ~/Documents/drill_items.org | grep "Question" | wc -l) "flash cards"
}

rszimg () {
    if [ $# -lt 2 ]; then
       echo "Usage: $0 <size> <img1> .. <imgn>"
    fi
    size=$1
    shift 1
    for f in "$@"; do
        cp "$f" "$f.bkup"
        convert "$f" -resize $size "$f";
    done
}

tpdfretit() {
    if [ $# -ne 2 ]; then
        echo "Usage: $0 <pdffile> <newtitle>"
        return 1
    fi

    fname="$1"
    newtitle="$2"
    #cp "$fname" "$fname.bkup"  # exiftool already makes a backup
    exiftool -Title="$newtitle" "$fname"
    mv "$fname" "${newtitle}.pdf"
    return 0
}

tlibretit() {
   for fname in *.pdf; do
        if echo "$fname" | grep " " >/dev/null; then
            sleep 0
            #echo -e "[SKIPPING]\t $fname";
            #continue
        fi
        echo "Current title is: $fname"
        echo -n "Retitle to: "
        read newname
        if [ -z "$newname" ]; then
            echo "Skipping."
            continue
        fi
        echo "Renaming $fname to $newname"
        tpdfretit "$fname" "$newname"
    done
    echo "Library renaming complete, enjoy."
}

printcolors() {
    for i in {0..254}; do
        print -P "$i: %F{$i}The quick brown fox, etc, etc.%f";
    done
}

psg() {
    ps -ef | grep -i $1
}

nsg() {
    netstat -natp | grep -i $1
}

# kubectl aliases
alias kc="kubectl"
alias kcc="kubectl config"
alias kcx="kubectl config use-context"

if [ -d ~/.kube ]; then
    export KUBECONFIG=$HOME/.kube/config:$(find $HOME/.kube/ -type f -name '*.yaml' | tr '\n' ':')
fi

kube_cache_file=~/.kube_cache

gen_kube_cache() {
    echo "Caching kubernetes namespace information."
    kubectl get all -A > ~/.kube_cache
}

get_kube_ns() {
    if [ -f $kube_cache_file ]; then
        cat $kube_cache_file | grep $1 | cut -d' ' -f1
    else
        kubectl get all -A | grep $1 | cut -d' ' -f1
    fi
}

kcsh() {
    echo "Shelling into pod $1"
    pod=$1
    ns=$(kubectl get pods -A | grep $pod | cut -d' ' -f1)
    kubectl exec --stdin --tty $pod -n $ns -- /bin/sh -c 'if which bash >&/dev/null; then bash; else /bin/sh; fi'
}

kcl() {
    echo "Getting logs for $1"
    ns=$(get_kube_ns $1)
    echo "Namespace is $ns"
    kubectl logs $1 -n $ns
}

kcy() {
    echo "Getting yaml for $1"
    line=$(kubectl get all -A | grep $1 | tr -s ' ') 
    ns=$(echo $line | cut -d' ' -f1)
    object=$(echo $line | cut -d' ' -f2)
    obj_type=$(echo $object | cut -d'/' -f1)
    obj_name=$(echo $object | cut -d'/' -f2)
    kubectl get $obj_type $obj_name -n $ns -oyaml
}

flag_file=~/.jump_flags
if [ ! -f "$flag_file" ]; then
    touch "$flag_file"
fi

# Easy navigation
set_flag() {
    flag="$1"
    dir="$(pwd)"
    sed -i $flag_file -e "/^$flag /d"
    echo "$flag $dir" >> $flag_file
}

jump_to_flag() {
    flag="$1"
    dir=$(cat $flag_file | grep -E "^$flag " | head -n1 | cut -d' ' -f2)
    if [ -z "$dir" ]; then
        echo "Flag $flag not found."
        return 1
    fi
    cd "$dir"
}

alias f=set_flag
alias j=jump_to_flag

alias dk=docker

dkl() {
    docker ps --format '{{.Names}}' | grep $1 | xargs -n1 docker logs
}

dkips() {
    for container in $(docker ps --format '{{.Names}}'); do
        echo -n $container "\t"
        docker inspect -f '{{range.NetworkSettings.Networks}} {{.IPAddress}}{{end}}' $container
    done
}

dklogs() {
    for container in $(docker container ls --format '{{.Names}}'); do
        echo "Getting logs for $container"
        docker logs $container >/tmp/$container
    done
}

dksh() {
    docker exec -it $1 sh -c 'bash || sh >&/dev/null'
}

dkps() {
    docker ps --format 'table {{.Names}}\t{{.Status}}\t{{.Ports}}'
}

loop() { while true; do $1; sleep 1; done  }

findi() {
    find . -iname "*$1*"
}

unix-name() {
    name="$*"
    name="$(echo "$name" | tr -d '!@#$%^&*()~,[]`' | tr -d "'" | tr -d '｜' | tr ' -' '__' | tr 'A-Z' 'a-z' | sed -E 's/_+/_/g')"
    echo "$name"
}

unixify-name() {
    name="$*"
    mv "$name" "$(unix-name $name)"
}

remove-last-ext() {
    echo "$*" | rev | cut -d'.' -f2- | rev
}

trim() {
    if [ -z "$1" ]; then
        sed 's/^[ \t]*//; s/[ \t]*$//' 
    else
        echo $1 | sed 's/^[ \t]*//; s/[ \t]*$//' 
    fi
}

backup() {
    if [ $# -lt 1 ]; then
        echo "Useage: $0 <files to backup>"
        return 1
    fi
    if [ ! -d ~/.backup ]; then mkdir ~/.backup; fi
    for f in $*; do
        echo "Backing up $f"
        cp -rv "$f" ~"/.backup/$f.$(date +%Y_%m_%d_%H_%M_%S).bkup"
    done
}

rundoom() {
    emacs --init-dir ~/.config/emacs
}

redoom() {
    mv ~/.emacs.d ~/emacs.d
}

undoom() {
    mv ~/emacs.d ~/.emacs.d
}

pirate() {
    # yt-dlp --extract-audio --audio-format mp3 "$1"
    yt-dlp --extract-audio --audio-format mp3 --download-sections "*0:00-20:00" "$1"
}

basicauth() {
    echo "$1:$2" | base64 | tr -d '\n'
}

zipr() {
    zip -r $1.zip $1 -x "*.pickle"
}

scoop-lab() {
    scp -rv lab:/home/almalinux/.cache/planner_prototype/experiments/$1 .
}
