(define-module (turthlib)
  #:export (export-all))

;; This has to be at the end of the file, not sure why yet.
(define (export-all)
  (module-map (lambda (sym var)
	      (module-export! (current-module) (list sym)))
	    (current-module)))

