;; keep package :init blocks small.
;; prefer to preserve package keybinds, for example, c-c c-x t -> spc c x t

(defalias 'map 'mapcar)
(defun mapmany (f &rest stuff)
  (map f stuff))

(global-set-key (kbd "<escape>") 'keyboard-escape-quit) ; so i can mindlessly spam esc
(global-set-key (kbd "C-S-v") 'evil-paste-before-cursor-after)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-i") 'imenu)
(global-set-key (kbd "C-h a") 'apropos-command)


(setq auto-save-default nil) ;; todo enable this
(setq backup-directory-alist '(("" . "~/.emacs.d/backup")))
(setq doc-view-continuous t)
(setq frame-background-mode 'dark)
(setq ielm-prompt "λ ")
(setq inferior-lisp-program (executable-find "sbcl"))
(setq lsp-pylsp-plugins-autopep8-enabled nil)
(setq lsp-pylsp-plugins-flake8-config "~/.config/flake8")
(setq lsp-pylsp-plugins-flake8-enabled t)
(setq lsp-pylsp-plugins-pycodestyle-enabled nil)
(setq lsp-pylsp-plugins-pydocstyle-enabled nil)
(setq lsp-pylsp-plugins-pyflakes-enabled nil)
(setq lsp-pylsp-plugins-pylint-enabled t)
(setq lsp-pylsp-plugins-yapf-enabled nil)
(setq lsp-pylsp-server-command "pylsp")
(setq ring-bell-function nil)
(setq rust-format-on-save t)
(setq scroll-conservatively 101) ;; if > 100, scroll, don't jump
(setq tmacs/doc-dir (if (file-exists-p "~/doc") "~/doc/" "~/Documents/")) ;; todo make the dir
(setq tmacs/fill-width 120)
(setq tmacs/zet-dir (concat tmacs/doc-dir "zet/"))
(setq sentence-end-double-space nil)
(setq geiser-default-implementation 'guile)
(setq geiser-active-implementations '(guile))
(setq scheme-program-name "guile")
(setq org-src-window-setup 'current-window)

(setq-default c-basic-offset 4)
(setq-default fill-column tmacs/fill-width) ;; Visual ruler
(setq-default indent-tabs-mode nil)
(setq-default tab-width 4)

(global-display-fill-column-indicator-mode 1)
(global-display-line-numbers-mode 1)
(global-visual-fill-column-mode 1)
(global-visual-line-mode 1)
(menu-bar-mode 0)
(scroll-bar-mode -1)
(set-fringe-mode 0)
(tool-bar-mode 0)
(tooltip-mode 1)
(electric-pair-mode 1)
(auto-revert-mode 1)

;; Markdown export
(require 'ox-md nil t)

(defun last-nth (n stuff)
  (nth (- (length stuff) n 1) stuff))

(setq tmacs/no-num-modes '(shell-mode-hook eshell-mode-hook term-mode-hook doc-view-mode-hook dashboard-mode-hook geiser-repl-mode-hook))
(defun tmacs/no-line-numbers () (display-line-numbers-mode 0))
(dolist (mode tmacs/no-num-modes)
  (add-hook mode 'tmacs/no-line-numbers))

(add-to-list 'Info-directory-list "~/.emacs.d/info")

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)

(use-package auctex :ensure t :pin gnu)
(use-package cargo)
(use-package counsel-projectile :config (counsel-projectile-mode))
(use-package dap-mode)
(use-package dashboard :custom (dashboard-center-content t) :init (dashboard-setup-startup-hook))
(use-package diminish)
(use-package doom-themes
  ;;:init (load-theme 'doom-one t)
  :init (load-theme 'doom-laserwave t)
  )
(use-package flycheck :config (setq flycheck-disabled-checkers '(emacs-lisp-checkdoc)))
(use-package lua-mode)
(use-package markdown-mode)
(use-package nyan-mode)
(use-package ob-rust)
(use-package org-recur)
(use-package org-tree-slide)
(use-package pyvenv)
(use-package rainbow-delimiters :hook (prog-mode . rainbow-delimiters-mode))
(use-package realgud)
(use-package realgud-lldb)
(use-package rust-mode)
(use-package rustic)
(use-package sicp)
(use-package slime :custom (inferior-lisp-program (executable-find "sbcl")))
(use-package swiper)
(use-package vimrc-mode)
(use-package xkcd
  :hook
  (xkcd-mode . (lambda () (interactive) (evil-local-mode -1))))
(use-package yaml-mode)
(use-package yasnippet :diminish yas-minor-mode :config (yas-reload-all))
(use-package yasnippet-snippets)
(use-package zig-mode)

(use-package general)
(general-create-definer tmacs/leader-keys
  :states '(normal visual insert emacs fundamental)
  :prefix "SPC"
  :global-prefix "C-SPC")

(use-package company
  :bind
  (:map company-active-map
        ("C-l" . company-complete-selection)
        ("<tab>" . company-complete-selection))
  :custom
  (company-minimum-prefix-length 2)
  (company-idle-delay 0.2))

(use-package counsel :diminish :init (counsel-mode 1))

(use-package evil
  :init
  (setq evil-toggle-key "C-<f1>")
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil) ;; Conflicts with collection
  :custom
  (evil-undo-system 'undo-tree)
  :config
  ;; (define-key evil-insert-state-map (kbd "M-u") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (evil-set-undo-system 'undo-tree)
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  :init
  (evil-mode 1))

(use-package evil-collection
  :after evil
  :config
  :init
  (define-key evil-motion-state-map (kbd "C-e") 'evil-scroll-up) ;; C-u is taken
  (evil-collection-init))

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-command] . helpful-command)
  ([remap describe-key] . helpful-key)
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-variable] . counsel-describe-variable))

(use-package ivy
  :diminish
  :bind
  (:map ivy-minibuffer-map
        ("TAB" . ivy-alt-done)
        ("C-<return>" . ivy-immediate-done)
        ("C-j" . ivy-next-line)
        ("C-k" . ivy-previous-line)
        ("M-j" . ivy-next-history-elment)
        ("M-k" . ivy-previous-line-or-history)
        ("C-l" . ivy-alt-done)
        ("C-u" . kill-whole-line) ; Why did't I think of this sooner?
        :map ivy-switch-buffer-map
        ("TAB" . ivy-done)
        ("C-<return>" . ivy-immediate-done)
        ("C-j" . ivy-next-line)
        ("C-k" . ivy-previous-line)
        ("C-l" . ivy-alt-done)
        ("C-d" . ivy-switch-buffer-killf)
        ("C-u" . kill-whole-line)
        :map ivy-reverse-i-search-map
        ("C-<return>" . ivy-immediate-done)
        ("C-k" . ivy-previous-line)
        ("C-l" . ivy-alt-done)
        ("C-d" . ivy-reverse-i-search-kill))
  :init
  (ivy-mode 1))

(defun tmacs/lsp-mode-hook ()
  (define-key lsp-mode-map (kbd "RET") nil)  ;; Turn off this stupid completion mechanism
  (lsp-enable-which-key-integration))

(use-package paredit
  :hook
  (emacs-lisp-mode . enable-paredit-mode)
  (scheme-mode . enable-paredit-mode))

(use-package lsp-mode
  :hook
  (lsp-mode . tmacs/lsp-mode-hook) (c-mode . lsp)
  (c++-mode . lsp)
  :bind
  ("M-RET" . lsp-execute-code-action) ; Alt enter will save the day :)
  :custom
  ;;(lsp-keymap-prefix "C-l")
  (lsp-enable-on-type-formatting nil)

  (setq lsp-clients-clangd-args
        '("--header-insertion-decorators=0")))

(use-package lsp-ui
  :custom
  (lsp-ui-doc-position 'at-point)
  (lsp-ui-sideline-show-diagnostics nil))

(use-package kotlin-mode
  :after (lsp-mode dap-mode)
  :config
  (require 'dap-kotlin)
  ;; should probably have been in dap-kotlin instead of lsp-kotlin
  (setq lsp-kotlin-debug-adapter-path "/opt/kotlin-debug-adapter/bin/kotlin-debug-adapter")
  ;;(setq lsp-kotlin-debug-adapter-path "")
  :hook
  (kotlin-mode . lsp))

(defun tmacs/org-hook ()
  (setq-local electric-pair-inhibit-predicate (lambda (c) (char-equal c ?<)))
  (setq evil-auto-indent nil))

(setq tmacs/org-babel-lanuages
      '((emacs-lisp . t)
        (C . t)
        (python . t)
        (makefile . t)
        (shell . t)
        (perl . t)
        (awk . t)
        (java . t)
        (js . t)
        (latex . t)
        (scheme . t)
        ;; (asm . t)
        (lua . t)))

(use-package org
  :hook
  (org-mode . tmacs/org-hook)
  :custom
  (org-clock-persist 'history)
  (org-agenda-span 'day)
  (org-agenda-log-mode-items '(closed clock))
  (org-log-done 'time)
  (org-log-into-drawer t)
  (advice-add 'org-refile :after 'org-save-all-org-buffers)
  (org-id-link-to-org-use-id 'use-existing)
  (org-habit-graph-column 40) ;;Column the habit graph shows up in.
  (org-startup-folded "content")
  (org-startup-with-inline-images t)
  (org-adapt-indentation t) ;;Indent to header level
  (org-clock-sound "~/Documents/zet/media/bell.wav")
  (org-refile-use-outline-path t)
  (org-babel-python-command "python3")
  (org-confirm-babel-evaluate nil)
  (org-src-tab-acts-natively t)
  (org-edit-src-content-indentation 0)
  (org-drill-cram-hours 240)
  (org-todo-keywords '((sequence "TODO(t)" "WAIT(w)" "|" "DONE(d)" "CNCL(c)")))
  :config
  (require 'org-habit)
  (require 'org-tempo)
  (add-to-list 'org-modules 'org-habit 'org-id)
  (org-defkey org-mode-map (kbd "C-c s") 'org-store-link)
  (org-clock-persistence-insinuate)
  (org-babel-do-load-languages 'org-babel-load-languages tmacs/org-babel-lanuages)
  (add-to-list 'org-latex-default-packages-alist '("" "mathtools" t))
  (add-to-list 'org-latex-default-packages-alist '("" "physics" t))
  ;; See if this improves speed
  ;;(add-to-list 'org-latex-default-packages-alist '("" "turthmath1" t))
  ;;(delete '("" "turthmath1" t) org-latex-default-packages-alist)
  (setq org-format-latex-options
        (plist-put org-format-latex-options :scale 2.0)))

(require 'ob)

;; Define a function to execute ASM code using NASM and LD
;; (defun org-babel-execute:asm (body params)
;;   "Execute a block of Asm code with org-babel.
;; This function is called by `org-babel-execute-src-block'."
;;   (let* ((src-file (org-babel-temp-file "asm-src-" ".asm"))
;;          (out-file (org-babel-temp-file "asm-out-" ".out"))
;;          (cmdline (concat "nasm -f elf64 " src-file " -o " src-file ".o && ld " src-file ".o -o " out-file " && " out-file)))
;;     ;; Write the body to the src-file.
;;     (with-temp-file src-file (insert body))
;;     ;; Execute the assembly and linking
;;    (org-babel-eval cmdline "")))

(setq tmacs/org-structure-templates
      '(("cl" . "src C")
        ("cpp" . "src C++")
        ("css" . "src css")
        ("el" . "src elisp")
        ("ht" . "src html")
        ("ja" . "src java")
        ("js" . "src js")
        ("la" . "src latex")
        ("lu" . "src lua")
        ("ma" . "src makefile")
        ("py" . "src python")
        ("pyr" . "src python :results output")
        ("ru" . "src rust")
        ("sc" . "src scheme")
        ("sh" . "src shell")
        ("vi" . "src vimrc")
        ("ya" . "src yaml")
        ("zi" . "src zig")))

(defun tmacs/org-src-flycheck ()
  (flycheck-mode 0))
(add-hook 'org-src-mode-hook 'tmacs/org-src-flycheck)

(dolist (template tmacs/org-structure-templates)
  (lambda (cell) (add-to-list 'org-structure-template-alist cell)))

(setq org-agenda-custom-commands
      '(("j" "Custom view"
         ((tags "+JOB=\"true\"")))))

(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory tmacs/zet-dir)
  (org-roam-capture-templates
   '(("d" "default" plain "%?"
      :target (file+head "${slug}.org" "#+title: ${title}\n%U")
      :unnarrowed t)))
  :config (org-roam-db-autosync-mode))

(use-package org-roam-ui
  :diminish
  :after org-roam
  ;; normally we'd recommend hooking orui after org-roam, but since org-roam does not have
  ;; a hookable mode anymore, you're advised to pick something yourself
  ;; if you don't care about startup time, use :hook (after-init . org-roam-ui-mode)
  :custom
  (org-roam-ui-sync-theme t)
  (org-roam-ui-follow t)
  (org-roam-ui-update-on-save t)
  (org-roam-ui-open-on-start nil))

(use-package projectile
  :diminish projectile-mode
  :custom
  ((projectile-completion-system 'ivy)
   (projectile-switch-project-action #'projectile-dired))
  :init
  (projectile-mode 1)
  (when (file-directory-p "~/devel")
    (setq projectile-project-search-path '("~/devel")))
  :bind-keymap
  ("C-c p" . projectile-command-map))

(use-package python-mode
  :hook
  (python-mode . lsp-mode)
  (python-mode . yas-minor-mode))

(tmacs/leader-keys
  :keymaps 'python-mode-map
  "x" '(lambda () (interactive) (progn (python-shell-send-buffer) (previous-window))))

(use-package undo-tree
  :diminish
  :custom
  (undo-tree-history-directory-alist '(("." . "~/.emacs.d/undo-tree/")))
  :config
  (global-undo-tree-mode 1))

(use-package visual-fill-column
  :custom
  (visual-fill-column-enable-sensible-window-split t)
  :hook
  (elfeed-show-mode . visual-fill-column-mode)
  :init
  (setq-default
   visual-fill-column-width tmacs/fill-width
   visual-fill-column-center-text t)
  (global-visual-fill-column-mode 1))

(use-package which-key
  :diminish
  :custom (which-key-idle-delay 0.5)
  :config (which-key-mode 1))

(defun tmacs/prog-mode-hook ()
  (company-mode 1)
  (setq fill-column tmacs/fill-width))
(add-hook 'prog-mode-hook 'tmacs/prog-mode-hook)

;; (require 'tex-site)
(setq TeX-auto-save t Tex-parse-self t)

;; From https://www.reddit.com/r/orgmode/comments/spip59/orgmode_stops_exporting_to_pdf/
(setq org-latex-pdf-process '("pdflatex -interaction nonstopmode -output-directory %o %f" "bibtex %b" "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f" "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

(defun tmacs/TeX-mode-hook ()
  (prettify-symbols-mode 1)
  (setq tex-fontify-script nil)
  (setq font-latex-fontify-script nil))

(add-hook 'TeX-mode-hook 'tmacs/TeX-mode-hook)
;;(add-hook 'doc-view-mode-hook (lambda () (local-set-key (kbd "SPC") nil)))
(defun tmacs/tetris-mode-hook () (visual-fill-column-mode 1))
(add-hook 'tetris-mode-hook 'tmacs/tetris-mode-hook)
(defun tmacs/doc-view-mode-hook () (blink-cursor-mode 0))
(add-hook 'doc-view-mode-hook 'tmacs/doc-view-mode-hook)

(add-hook 'rust-mode-hook (lambda () (yas-minor-mode-on) (lsp-lens-mode 0)))
(add-hook 'geiser-repl-mode-hook 'company-mode)

(general-define-key
 :keymaps 'doc-view-mode-map
 :states '(normal)
 "j" '(doc-view-next-line-or-next-page :which-key "next")
 "k" '(doc-view-previous-line-or-previous-page :which-key "prev"))

;; lsp
(general-define-key
 :keymaps 'lsp-mode-map
 :states '(normal insert)
 :prefix "C-l"
 "r" '(lsp-rename :which-key rename)
 "d" '(lsp-find-definition :which-key "find-definitions")
 "f" '(lsp-find-references :which-key "find-references")
 "h" '(lsp-describe-thing-at-point :which-key "help-detailed")
 "e" '(lsp-ui-flycheck-list :which-key "flycheck-list")
 "o" 'counsel-imenu
 "x" 'lsp-execute-code-action)

(general-define-key
 :definer 'minor-mode
 :keymaps 'org-tree-slide-mode
 :states '(normal visual insert emacs)
 "C-j" '(org-tree-slide-move-next-tree :which-key "next")
 "C-k" '(org-tree-slide-move-previous-tree :which-key "previous"))

(general-define-key
 :keymaps 'swiper-map
 "C-l" 'ivy-alt-done)

(general-define-key
 :keymaps 'tetris-mode-map
 :states 'normal
 "l" 'tetris-rotate-prev
 "h" 'tetris-rotate-next
 "k" 'tetris-move-down
 "k" 'tetris-move-down
 "a" 'tetris-move-left
 "s" 'tetris-move-down
 "d" 'tetris-move-right
 "j" 'tetris-move-bottom)

(general-define-key
 :keymaps 'org-agenda-mode-map
 "C-j" 'evil-next-visual-line
 "C-k" 'evil-previous-visual-line
 "j" 'evil-next-visual-line
 "k" 'evil-previous-visual-line)


(general-define-key
 :keymaps 'prog-mode-map
 :states 'normal
 "g[" 'flycheck-previous-error
 "g]" 'flycheck-next-error)


(tmacs/leader-keys
  "0" '(eshell :which-key "eshell")
  "]" '(other-window :which-key "other")
  "f-" '(previous-buffer :which-key "prev buffer")
  "f=" '(next-buffer :which-key "next buffer")
  ":" '(execute-extended-command-for-buffer :which-key "M-X")
  ";" '(counsel-M-x :which-key "M-x")
  "." '(shell-command :which-key "shell"))

(tmacs/leader-keys
  :keymaps '(python-mode-map)
  "r" '(python-shell-send-defun :which-key "eval defun")
  "R" '(python-shell-send-buffer :which-key "eval buffer"))

(tmacs/leader-keys
  :keymaps '(lisp-interaction-mode-map emacs-lisp-mode-map)
  "r" '(eval-defun :which-key "eval")
  "R" '(eval-buffer :which-key "buffer")
  "l" '(:ignore t :which-key "lisp")
  "lb" '(tmacs/eval-buffer :which-key "eval buffer")
  "ld" '(eval-defun :which-key "eval outermost form"))

(tmacs/leader-keys
  "f" '(:ignore t :which-key "find")
  "fb" '(ivy-switch-buffer :which-key "buffer")
  "fn" '(org-roam-node-find :which-key "note")
  "fs" '(:ignore t :which-key "special")
  "fp" '(projectile-find-file :which-key "find project")
  "ff" '(find-file :which-key "file"))

(tmacs/leader-keys "g" '(magit-status :which-key "magit"))
(tmacs/leader-keys "ec" '(whitespace-cleanup :which-key "whitespace"))
(tmacs/leader-keys "s" '(swiper :which-key "swiper"))

(tmacs/leader-keys
  "o" '(:ignore t :which-key "org")
  "ob" '(:ignore t :which-key "babel")
  "obr" '((lambda () (interactive) (progn (org-babel-tangle) (org-babel-execute-buffer))) :which-key "reload")
  "obt" '(org-babel-tangle :which-key "tangle")
  "obb" '(org-babel-execute-buffer :which-key "exec buffer")
  "os" '(org-schedule :which-key "schedule")
  "od" '(org-deadline :which-key "schedule")
  "oa" '(org-agenda :which-key "agenda")
  "oc" '(org-capture :which-key "capture")
  "of" '(org-drill :which-key "flash cards")
  ;;"ol" '(:ignore t :which-key "link")
  "oli" '(org-insert-link :which-key "insert")
  "ols" '(org-store-link :which-key "store")
  "og" '(:ignore t :which-key "go")
  "ogs" '(org-narrow-to-subtree :which-key "subtree")
  "ogw" '(widen :which-key "wide")
  "ok" '(org-toggle-checkbox :which-key "box")
  "om" '(:ignore t :which-key "timer")
  "oms" '(org-timer-set-timer :which-key "set")
  "om," '(org-timer-pause-or-continue :which-key "pause/continue")
  "omc" '(org-timer-stop :which-key "stop")
  "op" '(org-tree-slide-mode :which-key "present")
  "or" '(:ignore t :which-key "roam")
  "org" '(org-roam-graph :Which-key "graph")
  "orr" '(org-roam-capture :which-key "note")
  "orn" '(zet-org-roam-capture-random-node :which-key "note")
  ;;"orp" '(zet-purge :which-key "purge") ;; TODO Fix this
  "orta" '(org-roam-tag-add :which-key "add")
  "ortd" '(org-roam-tag-remove :which-key "delete")
  "orq" '(org-roam-dailies-capture-today :which-key "quick")
  "orm" '(org-roam-mode :which-key "roam mode")
  "ori" '(org-roam-node-insert :which-key "insert")
  "orb" '(zet-org-roam-capture-random-bib :which "buffer")
  "ord" '(org-roam-db-sync :which-key "db-sync")
  "orc" '(org-roam-capture :which-key "capture")
  "oru" '(org-roam-ui-open :which-key "ui")
  "ot" '(:ignore t :which-key "todo")
  "oty" '(org-todo-yesterday :which-key "yesterday")
  "ott" '(org-todo :which-key "todo")
  "oo" '(org-todo :which-key "todo")
  "ou" '(org-capture :which-key "capture")
  "ov" '(:ignore t :which-key "preview")
  "ovi" '(org-toggle-inline-images :which-key "images")
  "ovl" '(org-latex-preview :which-key "latex"))

(tmacs/leader-keys
  :keymaps 'org-mode-map
  "c" '(:ignore t :which-key "ctrl-c")
  "c'" '(org-edit-special :which-key "edit")
  "cc" '(org-ctrl-c-ctrl-c :which-key "ctrl-c")
  "ca" '(org-capture :which-key "capture")
  "cd" '(org-deadline :which-key "deadline")
  "cl" '(:ignore t :which-key "link")
  "cli" '(org-insert-link :which-key "insert")
  "cls" '(org-schedule :which-key "store")
  "cv" '(:ignore t :which-key "babel")
  "cvt" '(org-babel-tangle :which-key "tangle")
  "cvb" '(org-babel-execute-buffer :which-key "buffer")
  "cw" '(org-refile :which-key "refile")
  "cx" '(:ignore t :which-key "C-c x")
  "cxe" '(org-set-effort :which-key "set effort")
  "cxE" '(org-clock-modify-effort-estimate :which-key "clock modify effort")
  "cxp" '(org-set-property :which-key "property")
  "cxi" '(org-clock-in :which-key "clock-in")
  "cxo" '(org-clock-out :which-key "clock-out")

  "cp" '(:ignore t :which-key "project")
  "cpp" '(tmacs/set-project-property :which-key "set")
  "cpj" '(tmacs/set-job :which-key "job")
  "cpg" '(tmacs/visit-property-project :which-key "goto")
  "cpw" '(tmacs/refile-to-project :which-key "refile")

  "e" '(org-edit-special :which "edit")

  "i" '(:ignore t :which-key "insert")
  "ii" '(org-id-get-create :which-key "id")
  "in" '(org-indent-region :which-key "indent") ;; TODO improve this
  "it" '(:ignore t :which-key "timestamp")
  "ita" '(org-time-stamp :which-key "active")
  "iti" '(org-time-stamp-inactive :which-key "inactive")

  "r" '(org-babel-execute-src-block :which-key "run"))

(tmacs/leader-keys :definer 'minor-mode
  :keymaps 'org-capture-mode
  "d" '(org-capture-finalize :which-key "done")
  "k" '(org-capture-kill :which-key "kill"))

(defun rustc-and-run ()
  "Compile the current Rust file using `rustc` and run the resulting binary."
  (interactive)
  ;; Get the current file name and derive the output name.
  (let* ((source-file (buffer-file-name))
         (output-file (concat (file-name-directory source-file)
                              (file-name-base source-file))))
    ;; Compile the Rust file.
    (save-buffer)
    (compile (format "rustc %s -o %s && %s" source-file output-file output-file))))

(defun tmacs/cargo-run ()
  (interactive)
  (save-buffer)
  (if (equal "examples" (last-nth 1 (split-string default-directory "/")))
      (let ((example (nth 0 (split-string (buffer-name) "\\."))))
        (message (concat "Example is " example))
        (cargo-process-run-example example))
    (cargo-process-run)))

(defun tmacs/rustfmt ()
  (interactive)
  (save-buffer)
  (compile "cargo fmt")
  (revert-buffer t t)
  (save-buffer)
  (message "Formatted"))

(tmacs/leader-keys
  :keymaps 'rust-mode-map
  "tbf" '(tmacs/rustfmt :which-key "format")
  "x" '(rustc-and-run :which-key "rustc")
  "r" '(tmacs/cargo-run :which-key "cargo"))

(defun kotlinc-and-run ()
  (interactive)
  (let* ((source-file (buffer-file-name))
         (output-file (format "%sKt" (file-name-base source-file))))
    (save-buffer)
    (cond
     ((string-suffix-p ".kt" source-file)
      (compile (format "%s %s && /opt/kotlinc/bin/kotlin %s" kotlin-command source-file output-file)))
     ((string-suffix-p ".kts" source-file)
      (compile (format "%s -script %s" kotlin-command source-file))))))

(tmacs/leader-keys
  :keymaps 'kotlin-mode-map
  "x" '(kotlinc-and-run :which-key "exec"))

(tmacs/leader-keys
  "k" '(kill-this-buffer :which-key "kill buffer")
  "K" '(delete-file-and-buffer :which-key "delete file")
  "w" '(:ignore t :which-key "window")
  "wf" '(delete-other-windows :which-key "focus")
  "wh" '(evil-window-left :which-key "left")
  "wj" '(evil-window-down :which-key "down")
  "wk" '(evil-window-up :which-key "up")
  "wl" '(evil-window-right :which-key "right")
  "wd" '(delete-window :which-key "delete")
  "ws" '(:ignore t :which-key "split")
  "wsl" '(split-window-right :which-key "right")
  "w3" '(split-window-right :which-key "right")
  "w2" '(split-window-below :which-key "down")
  "wsj" '(split-window-below :which-key "down"))

(tmacs/leader-keys
  "t" '(:ignore t :which-key "tmacs")
  "tw" '(save-buffer :which-key "write")
  "td" '((lambda () (interactive)
           (setq debug-on-error (not debug-on-error))
           (message (format "Debug on error is now %s" debug-on-error)))
         :which-key "debug")
  "tc" '(whitespace-cleanup :which-key "cleanup")
  "tb" '(:ignore t :which-key "buffer")
  "tt" '(:ignore t :which-key "theme")
  "ttl" '(load-theme :which-key "load")
  "tbc" '(tmacs/replace-buffer-with-clipboard :which-key "clipboard")
  "tfs" '(tmacs/set-font-size :which-key "font")
  "tp" '(tmacs/print-buffer-as-a5 :which-key "print")
  "tfn" '(tmacs/rename-current-buffer-file :which-key "rename"))

(tmacs/leader-keys
  :keymaps 'org-mode-map
  "tbf" '(tmacs/org-reindent-buffer :which-key "format"))

(tmacs/leader-keys
  :keymaps 'geiser-mode-map
  "r" '(geiser-eval-definition :which-key "eval"))

(tmacs/leader-keys
  :keymaps 'scheme-mode-map
  "tbf" '(indent-buffer :which-key "indent"))

(tmacs/leader-keys
  :keymaps 'emacs-lisp-mode-map
  "tbf" '((lambda () (interactive) (indent-region (point-min) (point-max))) :which-key "indent"))

(tmacs/leader-keys
  :states 'normal
  "SPC" '(eval-last-sexp :which-key "eval"))


(defun load-if-exists (file)
  (if (file-exists-p file)
      (load-file file)))

(load-if-exists "~/.emacs.d/tmacs.el")
(load-if-exists "~/devel/zet_mode/zet-mode.el")


(let ((local-file (concat "~/.emacs.d/" (system-name) ".el"))
      (known-hosts '("mithaokhta" "cat11479")))
  (if (and (file-exists-p local-file) (member (system-name) known-hosts))
      (load-file local-file)))

;; Redefine buggy function, breaks on (cons 1 2)
(defun org-babel-scheme--table-or-string (results) results)

(defun indent-buffer ()
  (interactive)
  (indent-region (point-min) (point-max)))

(add-to-list 'org-structure-template-alist '("sch" . "src scheme"))

(set-face-attribute 'flycheck-error nil :underline '(:color "red" :style line) :box nil)
(set-face-attribute 'flycheck-warning nil :underline '(:color "yellow" :style line) :box nil)

(eval-after-load 'info
  '(define-key Info-mode-map (kbd "SPC") nil))

(global-set-key (kbd "C-=") 'evil-local-mode)
(global-set-key (kbd "M-u") 'previous-buffer)

(defun hm (hours minutes)
  (+ minutes (* 60.0 hours)))

(yas-define-snippets
 'org-mode
 (list
  '("<reso" "reults output")))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(evil-collection xkcd auctex zig-mode yasnippet-snippets yaml-mode which-key web-mode visual-fill-column vimrc-mode undo-tree slime sicp rustic realgud-lldb rainbow-delimiters pyvenv python-mode plantuml-mode paredit org-tree-slide org-roam-ui org-recur org-drill ob-rust nyan-mode lua-mode lsp-ui kotlin-mode helpful general geiser-mit geiser-guile format-all flycheck elfeed doom-themes diminish dashboard dap-mode counsel-projectile company cargo)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
