(defun tmacs/generate-random-string (length)
  "Generate a random string of a given LENGTH."
  (let ((charset "abcdefghijklmnopqrstuvwxyz")
        (result ""))
    (dotimes (_ length result)
      (setq result (concat result (string (elt charset (random (length charset)))))))))

(defun tmacs/org-roam-capture-random-node ()
  "Capture a new Org-roam node with a random 6-character name."
  (interactive)
  (let ((title (concat "zet-" (tmacs/generate-random-string 6))))
    (org-roam-capture- :node (org-roam-node-create :title title))))

(defun zet--truncated-file-name (filename)
  (substring filename 0 10))

(defun zet-make-filename-map ()
  (setq zet--filename-map (make-hash-table :test 'equal))
  (dolist (file (directory-files zet-dir nil "zet_*"))
    (puthash (zet--truncated-file-name file) file zet--filename-map)))

(defun zet--display-filename (shortened)
  (if (string-equal zet--from-link shortened)
      (format "%s *\n" shortened)
    (format "%s\n" shortened)))

(defun zet-initialize ()
  (interactive)
  (setq zet--from-link nil)
  (zet-show-files))

(defun zet-show-files ()
  "Display the names of all files in the current directory matching the pattern 'zet-' in a vertically split window."
  (interactive)
  (let ((buffer (get-buffer-create "*Zet Files*")))
    (zet-make-filename-map)
    (with-current-buffer buffer
      (setq buffer-read-only nil)
      (erase-buffer)
      (maphash (lambda (k v) (insert (zet--display-filename k)))
               zet--filename-map)
      (setq buffer-read-only t)
      (goto-char (point-min))
      (zet-mode))
    (delete-other-windows)
    (split-window-right)
    (set-window-buffer (selected-window) buffer)
    (other-window 1)
    (let ((content-buffer (get-buffer-create "*Zet File Contents*")))
      (set-window-buffer (selected-window) content-buffer)
      (with-current-buffer content-buffer
        (org-mode)))
    (other-window 1)
    (zet-disable-evil)))

(defun zet-mark-from ()
  (interactive)
  (setq zet--from-link (zet--extract-file-key))
  (message "Zet from is now %s" zet--from-link)
  (zet-show-files))

(defvar zet-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "C-n") 'next-line)
    (define-key map (kbd "C-p") 'previous-line)
    (define-key map (kbd "j") 'next-line)
    (define-key map (kbd "k") 'previous-line)
    (define-key map (kbd "a") 'zet-mark-from)
    (define-key map (kbd "RET") 'zet-mark-from)
    map)
  "Keymap for `zet-mode'.")

(define-derived-mode zet-mode special-mode "Zet"
  "Major mode for displaying zet files."
  (add-hook 'post-command-hook 'zet-show-file-contents nil t))

(defun zet-disable-evil ()
  (interactive)
  (when (eq major-mode 'zet-mode)
    (message "Current mode is zet-mode, disabling evil")
    (if (bound-and-true-p evil-mode)
        (progn
          (message "Evil is currently enabled")
          (evil-local-mode -1)
          (message "Evil disabled"))
      (message "Evil not enabled"))))

(defun zet--line-to-map-key (line)
  (substring line 0 -1))

(defun remove-newlines (str)
  "Remove all newlines from the given string STR."
  (replace-regexp-in-string "\n" "" str))

(defun zet--text-object-under-point ()
  "Get the text object under point, extending but not crossing line boundaries or whitespace."
  (interactive)
  (let ((start (point))
        (end (point))
        (original (point)))
    ;; Move start backward to the beginning of the text object
    (while (and (not (bolp))
                (not (looking-back "[ \t\n]" 1)))
      (setq start (1- start))
      (goto-char start))
    (setq start (point))

    ;; Move end forward to the end of the text object
    (goto-char end)
    (while (and (not (eolp))
                (not (looking-at "[ \t\n]")))
      (setq end (1+ end))
      (goto-char end))
    (setq end (point))

    (goto-char original)
    ;; Return the text object as a string
    (buffer-substring-no-properties start end)))

(defun zet--extract-file-key ()
  (remove-newlines (zet--text-object-under-point)))

(defun zet-show-file-contents ()
  "Show contents of the file under the cursor in the right window."
  (interactive)
  (let ((file (gethash (zet--extract-file-key) zet--filename-map)))
    (if (not file)
        (message (format "No such key %s" (zet--extract-file-key))))
    (when file
      (message "Showing %s" file)
      (setq file (string-trim file))
      (let ((buffer (get-buffer-create "*Zet File Contents*")))
        (with-current-buffer buffer
          (setq buffer-read-only nil)
          (erase-buffer)
          (insert-file-contents (concat zet-dir "/" file))
          (setq buffer-read-only t))
        (set-window-buffer (next-window) buffer)))))

(global-set-key (kbd "C-c z") 'zet-initialize)

