(setq len 'length)
(setq filter 'cl-remove-if-not-condition)
(defun filter-not-nil (lst) (filter (lambda (x) (not (eq x nil))) lst))
(defun lines (str) (split-string str "\n" t))
(defun buffer-lines () (lines (buffer-string)))
(defun file-string (filename) (with-temp-buffer (insert-file-contents filename) (buffer-string)))
(defun file-lines (filename) (lines (file-string filename)))
(defun choose (lst) (nth (random (length lst)) lst))
(defun current-line () (thing-at-point 'line t))

(defun count (element lst)
  (length (filter (lambda (el) (equal el element)) lst)))

(defun fib (n)
  (cond ((<= n 1) 1)
        (t (+ (fib (- n 1)) (fib (- n 2))))))

(defun range (low high)
  (cond ((< high low) '())
        (t (append (range low (- high 1)) (list high)))))

(defun in-range (min max number)
  (and (<= min number) (<= number max)))

(defun chars (str)
  (mapcar 'char-to-string str))

(defun teleprint (str)
  (with-output-to-temp-buffer "*output*"
    (switch-to-buffer "*output*")
    (dolist (char (to-chars str))
      (insert char)
      (sit-for 0.05))))

(defmacro symbolize (symbol)
  "Define STR to be its own string representation"
  (message "Symbolizing %s" symbol)
  `(defvar ,symbol (symbol-name ',symbol)))

(defun speedup-keyboard () (interactive) (shell-command "xset r rate 200 50"))

(defun tmacs/rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

(defun tmacs/org-reindent-buffer ()
  "Reindent the entire Org mode buffer according to Org mode's indentation rules."
  (interactive)
  (org-indent-region (point-min) (point-max))
  (org-cleanup)
  (save-buffer))

(defun tmacs/set-font-size ()
  "Set font size for current buffer."
  (interactive)
  (setq font-size (read-number "Enter font size: "))
  (set-frame-font (concat "hack " (number-to-string font-size))))

(defun tmacs/replace-buffer-with-clipboard ()
  "Replace the contents of the buffer with the contents of the clipboard."
  (interactive)
  (delete-region (point-min) (point-max))
  (insert (gui-get-primary-selection)))

(defun tmacs/set-project-property ()
  (interactive)
  (org-set-property
   "PROJECT"
   (org-roam-node-id
    (org-roam-node-read))))

(defun tmacs/set-job ()
  (interactive)
  (org-set-property "JOB" "true"))

(defun tmacs/visit-property-project ()
  (interactive)
  (let ((project-id (org-entry-get nil "PROJECT")))
    (message (format "Project ID is %s" project-id))
    (if project-id
        (let ((node (car (org-roam-id-find project-id))))
          (message (format "Node is %s" node))
          (find-file node)))))

(defun tmacs/refile-to-project () (interactive)
  (let ((project-id (org-entry-get nil "PROJECT")))
    (if project-id
        (let* ((filename (car (org-roam-id-find project-id)))
               (org-refile-targets '((filename :maxlevel . 1))))
          (message (format "Filename is %s" filename))
          (org-refile-cache-clear)
          (org-refile))
      (message (format "This node does not have a :PROJECT:")))))

(defun tmacs/eval-buffer ()
  (interactive)
  (eval-buffer)
  (message "Evaluated buffer"))

(defun org-id-link-regex (org-id)
  (format "\\[\\[%s\\]\\[\\(.*?\\)\\]\\]" org-id))

(defun tmacs/grep-documents-for-string (search-string)
  "Search for SEARCH-STRING in all files in the ~/Documents directory using grep and return a list of files containing the string."
  (interactive "sEnter search string: ")
  (let* ((grep-command (concat "grep -rl --include='*' \"" search-string "\" ~/Documents"))
         (grep-output (shell-command-to-string grep-command))
         (file-list (split-string grep-output "\n" t)))
    file-list))

(defun tmacs/delete-links-to-file ()
  (interactive)
  (let ((org-id (org-id-store-link)) (links-deleted 0))
    (if org-id
        (progn
          (message (format "ID is %s" org-id))
          (if (y-or-n-p (format "Really delete %s?" org-id))
              (save-excursion
                (dolist (org-file (tmacs/grep-documents-for-string org-id))
                  (message (concat "Deleting from " org-file))
                  (find-file org-file)
                  (goto-char (point-min))
                  (while (re-search-forward (org-id-link-regex org-id) nil t)
                    (replace-match "\\1")
                    (setq links-deleted (+ links-deleted 1)))
                  (message (format "Deleted links from %s" (buffer-name)))
                  (save-buffer))
                (message (format "Deleted %d links" links-deleted)))))
      (message "This file has no org ID"))))

(defun tmacs/purge-note ()
  (interactive)
  (tmacs/delete-links-to-file)
  (delete-file-and-buffer))

(defun tmacs/print-buffer-as-a5 ()
  "Print the current buffer as A5 size using lp command."
  (interactive)
  (let ((temp-file (make-temp-file "emacs-print-" nil ".txt")))
    (write-region (point-min) (point-max) temp-file)
    (shell-command (format "a2ps -1 -f8 -MA5 --no-header --borders=no --margin=0 -o %s.ps %s && lp %s.ps -omedia=a5" temp-file temp-file temp-file))
    (delete-file temp-file)
    (message "Sent buffer to lp")))

(defun seconds-to-stamp (seconds)
  (let* ((seconds-remainder (% seconds 60))
         (minutes-remainder (% (/ (- seconds seconds-remainder) 60) 60))
         (hours-remainder (/ (- seconds seconds-remainder (* minutes-remainder 60))
                             (* 60 60))))
    (format "%02d:%02d:%02d" hours-remainder minutes-remainder seconds-remainder)))

(defun time-since-last-active-timestamp ()
  "Search for all active timestamps in the current Org mode file and report the time since the last timestamp."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (let ((last-timestamp nil))
      (while (re-search-forward
              "\\([0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\} [a-zA-Z]+ [0-9]\\{2\\}:[0-9]\\{2\\}\\)"
              nil t)
        (let ((timestamp (match-string 1)))
          (when t
            (setq last-timestamp
                  (if (and last-timestamp
                           (time-less-p (org-time-string-to-time timestamp)
                                        (org-time-string-to-time last-timestamp)))
                      last-timestamp
                    timestamp)))))
      (if last-timestamp
          (progn
            (message "Time since last timestamp: %s"
                     (seconds-to-stamp (time-subtract (time-convert (current-time) 1)
                                                      (time-convert (org-time-string-to-time last-timestamp) 1)))))
        (message "No active timestamps found.")))))

(defun org-sum-time-across-headlines ()
 "Sum up all the time durations in org-mode headlines."
  (interactive)
  (let ((total 0)
        (time-regexp "=>  \\([0-9]+\\):\\([0-9]+\\)")) ; This will match HH:MM format
    (save-excursion
      (goto-char (point-min))
      ;; Search for the time-regexp across all headlines
      (while (re-search-forward time-regexp nil t)
        (let ((hours (string-to-number (match-string 1)))
              (minutes (string-to-number (match-string 2))))
          (setq total (+ total minutes (* 60 hours))))))
    ;; Convert total minutes back to hours:minutes and display
    (message "Total time clocked: %d:%02d" (/ total 60) (% total 60))))

;; based on http://emacsredux.com/blog/2013/04/03/delete-file-and-buffer/
(defun delete-file-and-buffer ()
  "Kill the current buffer and deletes the file it is visiting."
  (interactive)
  (let ((filename (buffer-file-name)))
    (if filename
        (if (y-or-n-p (concat "Do you really want to delete file " filename " ?"))
            (progn
              (delete-file filename)
              (message "Deleted file %s." filename)
              (kill-buffer)))
      (message "Not a file visiting buffer!"))))

(defun tmacs/org-print-visible ()
  (interactive)
  (org-copy-visible (point-min) (point-max))
  (with-temp-buffer
    (yank)
    (tmacs/print-buffer-as-a5)))

(defun line-empty-p (n)
  " Check if line `n' offset from the point is empty."
  (save-excursion
      (beginning-of-line (+ n 1))
      (if (and (> n 0) (eobp))
          nil
        (looking-at-p "^[ \t]*$"))))

(defun delete-next-line ()
  (save-excursion
    (forward-line)
    (delete-line)))

(defun delete-previous-line ()
  (save-excursion
    (previous-line)
    (delete-line)))

(defun org-cleanup ()
  "Lint an org mode file."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward org-heading-regexp nil t)
      (message "Found heading on line %d" (line-number-at-pos))
      (while (line-empty-p 1)
        (delete-next-line))
      (save-excursion
        (when (re-search-forward org-heading-regexp nil t)
          (while (and (line-empty-p -1) (line-empty-p -2))
            (delete-previous-line))
          (when (not (line-empty-p -1))
            (previous-line)
            (end-of-line)
            (insert "\n")))))))

(defun tmacs/org-get-top-level-heading-above-point ()
  "Get the top level org heading above the point"
  (interactive)
  (if (org-before-first-heading-p)
      (error "Before first headline"))
  (save-excursion
    (while (not (or (= (org-outline-level) 1)
                    (bobp)))
      (outline-previous-heading))
    (if (and (= (org-outline-level) 1)
             (org-at-heading-p))
        (org-get-heading t t t t)
      (error "No heading above point"))))

(defun tmacs/org-copy-link-at-point ()
  "Copy the Org link at point to the kill ring."
  (interactive)
  (let ((link (org-element-context)))
    (if (eq (car link) 'link)
        (let ((url (org-element-property :raw-link link)))
          (kill-new url)
          (message "Copied link: %s" url))
      (message "No link at point"))))

(defun tmacs/archive-closed (search-file)
  (save-excursion
    (find-file (tmacs/zet-file "scratch"))
    (goto-char (point-min))
    (while (re-search-forward org-heading-regexp nil t)
      (beginning-of-line) ;; Redundant?
      (when (looking-at-p "\\** DONE .*")
        (message "Found closed headline %s" (current-line))
        (org-refile nil
                    nil
                    (list "Closed/ (closed.org)"
                          (tmacs/zet-file search-file)
                          nil ;; 108 is hardcoded headline position, bad
                          108)))
      (forward-line 1))))

(defun filter-control-sequences (process output)
  (let ((clean-output (replace-regexp-in-string "\x1b\\[[0-9;]*[a-zA-Z]" "" output)))
    (setq clean-output (replace-regexp-in-string "\r" "\n" clean-output))
    (with-current-buffer (process-buffer process)
      (insert clean-output))))

(defmacro shellout (command &rest args)
  `(let ((procname ,(string-replace " " "-" command))
         (buffer ,(get-buffer-create "*shellout*")))
     (start-process-shell-command procname
                                  buffer
                                  (format "%s %s" ,command (mapconcat 'identity (list ,@args) " ")))
     (set-process-filter (get-process procname) 'filter-control-sequences)
     (display-buffer buffer)))

(defun eshell-clear-line ()
  (interactive)
  (let ((inhibit-read-only t))
    (eshell-bol)
    (kill-line)))

(defun eshell-setup-keybingds ()
  (define-key eshell-mode-map (kbd "C-'") 'eshell-clear-line))

(setq zet-dir "~/Documents/zet")

(defmacro tmacs/find-file (filepath which-key)
  `(quote ((lambda () (interactive) (find-file ,filepath)) :which-key ,which-key)))

(defun without-suffix (suffix string)
  (if (string-suffix-p suffix string)
      (substring string 0 (- (length string) (length suffix)))
    string))

(defun remove-newlines (string)
  (string-replace "\n" "" string))

(defun basic-auth (username password)
  (remove-newlines
   (base64-encode-string
    (concat username ":" password))))

(defun generate-uuid ()
  "Generate a UUID and return it as an ASCII string."
  (string-trim (shell-command-to-string "uuidgen")))
